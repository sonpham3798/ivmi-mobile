import React from 'react';
import {Icon} from '@ui-kitten/components';

interface Props {
  size?: number;
  color: string;
  name: string;
}

export default function IvmiIcon({size = 27, color, name}: Props) {
  return <Icon style={{width: size, height: size}} fill={color} name={name} />;
}
