import React from 'react';
import {Image, ImageSourcePropType, ImageStyle} from 'react-native';

interface Props {
  style?: ImageStyle;
  src: ImageSourcePropType;
  size?: number;
}

export default function IvmiImageIcon({src, size = 27, style}: Props) {
  return <Image source={src} style={{...style, width: size, height: size}} />;
}
