import React, {ReactElement} from 'react';
import {TouchableOpacity, View} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {Layout, Divider} from '@ui-kitten/components';
import IvmiText from './IvmiText';

interface Props {
  icon: ReactElement;
  title: string;
  time: string;
  content: string;
  unread: boolean;
}

export default function IvmiNotiItem({
  icon,
  title,
  time,
  content,
  unread,
}: Props) {
  const navigation = useNavigation();
  return (
    <Layout style={{width: '100%', alignItems: 'center'}}>
      {/* <View style={{width: '95%', marginBottom: 10}}> */}
      {/* <TouchableOpacity
          activeOpacity={0.3}
          onPress={() => navigation.navigate('NotificationDetails')}> */}
      <Layout
        level="3"
        style={{
          overflow: 'hidden',
          borderRadius: 10,
          paddingVertical: 5,
          flexDirection: 'row',
          alignItems: 'center',
        }}>
        <Layout level="3" style={{width: '15%', alignItems: 'center'}}>
          {icon}
        </Layout>
        <Layout level="3" style={{width: '85%'}}>
          <IvmiText fontStyle={unread ? 'bold' : 'regular'}>{title}</IvmiText>
          <Divider
            style={{
              backgroundColor: 'rgba(0,0,0,0.2)',
              marginTop: 3,
              width: '100%',
            }}
          />
          <IvmiText fontStyle={unread ? 'bold' : 'regular'}>{content}</IvmiText>
          <IvmiText
            style={{fontSize: 12, marginTop: 10}}
            fontStyle={unread ? 'bold-italic' : 'italic'}>
            {time}
          </IvmiText>
        </Layout>
      </Layout>
      {/* </TouchableOpacity> */}
      {/* </View> */}
    </Layout>
  );
}
