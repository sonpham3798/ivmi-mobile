import React, {ReactElement} from 'react';
import {View, Text} from 'react-native';
import {ListItem} from '@ui-kitten/components';
import IvmiIcon from './IvmiIcon';
import IvmiText from './IvmiText';
import {useNavigation} from '@react-navigation/native';

interface Props {
  name: string;
  leftIcon?: ReactElement;
  rightIcon?: ReactElement;
  to: string;
}

export default function IvmiProfileItem({
  name,
  leftIcon = <></>,
  rightIcon = (
    <IvmiIcon
      name="arrow-ios-forward-outline"
      size={20}
      color="rgb(167, 180, 204)"
    />
  ),
  to,
}: Props) {
  const navigation = useNavigation();
  return (
    <ListItem
      onPress={() => navigation.navigate(to)}
      title={() => <IvmiText style={{marginLeft: 10}}>{name}</IvmiText>}
      accessoryLeft={() => leftIcon}
      accessoryRight={() => rightIcon}
    />
  );
}
