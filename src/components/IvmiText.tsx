import React, {ReactChild} from 'react';
import {Text, TextStyle, TextProperties} from 'react-native';

interface Props extends TextProperties {
  style?: TextStyle;
  fontStyle?:
    | 'regular'
    | 'italic'
    | 'medium'
    | 'medium-italic'
    | 'bold'
    | 'bold-italic';
  children: ReactChild;
}

const mapToFontName = {
  regular: 'CerebriSans-R',
  italic: 'CerebriSans-I',
  medium: 'CerebriSans-M',
  'medium-italic': 'CerebriSans-MI',
  bold: 'CerebriSans-B',
  'bold-italic': 'CerebriSans-BI',
};

export default function IvmiText({
  style,
  fontStyle = 'regular',
  children,
  ...props
}: Props) {
  return (
    <Text
      style={{
        ...style,
        fontSize: style?.fontSize || 15,
        fontFamily: mapToFontName[fontStyle],
      }}
      {...props}>
      {children}
    </Text>
  );
}
