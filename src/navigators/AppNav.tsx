import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';

import {HomeNav} from './HomeNav';
import Login from '../screens/Login';
import Details from '../screens/noti/Details';

const {Navigator, Screen} = createStackNavigator();

const TabNavigator = () => (
  <Navigator initialRouteName="Login" headerMode="none">
    <Screen name="Login" component={Login} />
    <Screen name="Home" component={HomeNav} />

    <Screen name="NotificationDetails" component={Details} />
  </Navigator>
);

export const AppNav = () => <TabNavigator />;
