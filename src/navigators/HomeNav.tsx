import React, {FC} from 'react';
import {
  createMaterialTopTabNavigator,
  MaterialTopTabBarProps,
} from '@react-navigation/material-top-tabs';
import Notification from '../screens/noti/Notification';
import Garden from '../screens/gard/Garden';
import {View} from 'react-native';
import {SettingsNav} from './SettingsNav';
import IvmiText from '../components/IvmiText';
import IvmiImageIcon from '../components/IvmiImageIcon';
import {TouchableNativeFeedback} from 'react-native-gesture-handler';

const {Navigator, Screen} = createMaterialTopTabNavigator();

const TopTabBar: FC<MaterialTopTabBarProps> = ({navigation, state}) => (
  <View
    style={{
      flexDirection: 'row',
      backgroundColor: 'white',
      shadowColor: '#000',
      shadowOffset: {
        width: 0,
        height: -19,
      },
      shadowOpacity: 0.5,
      shadowRadius: 12.35,

      elevation: 19,
    }}>
    {state.routes.map((route: any, index: number) => {
      const label = route.name;
      const isFocused = state.index === index;

      const onPress = () => {
        const event = navigation.emit({
          type: 'tabPress',
          target: route.key,
          canPreventDefault: true,
        });

        if (!isFocused && !event.defaultPrevented) {
          navigation.navigate(route.name);
        }
      };

      const onLongPress = () => {
        navigation.emit({
          type: 'tabLongPress',
          target: route.key,
        });
      };

      const iconSrc =
        label == 'Garden'
          ? require('../../assets/icons/garden.png')
          : label == 'Notification'
          ? require('../../assets/icons/notification.png')
          : require('../../assets/icons/settings.png');

      return (
        <View
          key={index}
          style={{
            width: isFocused ? '50%' : '25%',
            height: 55,
            justifyContent: 'center',
            overflow: 'hidden',
          }}>
          {isFocused ? (
            <View
              style={{
                alignItems: 'center',
              }}>
              <View
                style={{
                  borderRadius: 20,
                  overflow: 'hidden',
                }}>
                <TouchableNativeFeedback
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-evenly',
                    alignItems: 'center',
                    backgroundColor: '#B3FFD6',
                    paddingHorizontal: 10,
                    paddingVertical: 5,
                    minWidth: '65%',
                  }}>
                  <IvmiImageIcon src={iconSrc} size={25} />
                  <IvmiText style={{fontSize: 14}} fontStyle="medium">
                    {label}
                  </IvmiText>
                </TouchableNativeFeedback>
              </View>
            </View>
          ) : (
            <View
              style={{
                height: 90,
                borderRadius: 45,
                overflow: 'hidden',
              }}>
              <TouchableNativeFeedback onPress={onPress}>
                <View
                  style={{
                    width: '100%',
                    height: '100%',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <IvmiImageIcon src={iconSrc} size={25} />
                </View>
              </TouchableNativeFeedback>
            </View>
          )}
        </View>
      );
    })}
  </View>
);

export const HomeNav = () => (
  <Navigator
    tabBarPosition="bottom"
    tabBar={(props: MaterialTopTabBarProps) => <TopTabBar {...props} />}>
    <Screen name="Garden" component={Garden} />
    <Screen name="Notification" component={Notification} />
    <Screen name="Settings" component={SettingsNav} />
  </Navigator>
);
