import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';

import Settings from '../screens/sett/Settings';
import Profile from '../screens/sett/Profile';
import {View, ImageBackground} from 'react-native';
import IvmiText from '../components/IvmiText';
import Information from '../screens/sett/Information';

const {Navigator, Screen} = createStackNavigator();

const TabNavigator = () => (
  <>
    <View
      style={{
        width: '100%',
        height: 170,
        backgroundColor: 'black',
        shadowColor: '#000',
        shadowOffset: {
          width: 0,
          height: 9,
        },
        shadowOpacity: 0.5,
        shadowRadius: 12.35,

        elevation: 19,
      }}>
      <ImageBackground
        style={{
          flex: 1,
          opacity: 0.7,
          backgroundColor: 'black',
          alignItems: 'center',
          paddingTop: 3,
        }}
        source={{
          uri:
            'https://hips.hearstapps.com/ghk.h-cdn.co/assets/15/33/1439490128-plants.jpg',
        }}>
        <IvmiText fontStyle="medium" style={{color: 'white', fontSize: 23}}>
          Settings
        </IvmiText>
      </ImageBackground>
    </View>
    <Navigator initialRouteName="Settings" headerMode="none">
      <Screen name="Settings" component={Settings} />
      <Screen name="Profile" component={Profile} />
      <Screen name="Information" component={Information} />
    </Navigator>
  </>
);

export const SettingsNav = () => <TabNavigator />;
