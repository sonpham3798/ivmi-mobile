import React from 'react';
import {Text, Layout, Input, Button} from '@ui-kitten/components';
import {StyleSheet} from 'react-native';

const Login = ({navigation}: {navigation: any}) => {
  const [username, setUsername] = React.useState('');
  const [password, setPassword] = React.useState('');
  const [hidePassword, setHidePassword] = React.useState(true);

  return (
    <Layout style={styles.container}>
      <Layout style={styles.layoutLogin}>
        <Text style={styles.text} category="h1">
          IVMI
        </Text>
        <Layout style={styles.form}>
          <Input
            label="Username"
            focusable={true}
            size="large"
            value={username}
            onChangeText={(value) => setUsername(value)}
          />
          <Input
            label="Password"
            secureTextEntry={hidePassword}
            size="large"
            value={password}
            onChangeText={(value) => setPassword(value)}
          />
          <Button
            style={styles.button}
            onPress={() => navigation.navigate('Home')}>
            Login
          </Button>
        </Layout>
      </Layout>
    </Layout>
  );
};

const sigin = () => {
  // just move to Home screen
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    paddingHorizontal: 24,
  },
  layoutLogin: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  form: {
    marginTop: 24,
    width: '100%',
  },
  text: {
    //
  },
  button: {
    marginTop: 12,
  },
});

export default Login;
