import React from 'react';
import {Layout, List, Text, Divider, Button, StyleService, Card} from '@ui-kitten/components';
import {TouchableOpacity, StyleSheet} from 'react-native';
import IvmiImageIcon from '../components/IvmiImageIcon';

import Settings from '../screens/Settings';

const vegetableThumbnail = [
  'http://aslan.dev/go/VBlujpmf4',
  'http://aslan.dev/go/2hnJX3xJp',
  'http://aslan.dev/go/2bqtidqJv'
]

const data: Array<{}> = [
  {
    line: 1,
    vegetable: 'Broccoli',
    available: 1,
    plots: 5,
    status: "Harvest",
    thumbnail: 0,
    notification: 'Broccoli seems to be able to harvest.',
  },
  {
    line: 2,
    vegetable: 'Brassica integrifolia',
    available: 5,
    plots: 5,
    status: "Normal",
    thumbnail: 1,
  },
  {
    line: 3,
    vegetable: 'Salad',
    available: 5,
    plots: 5,
    status: "Disease",
    thumbnail: 2,
    notification: 'Salad may be have yellow leaves diseases.',
  },
];

const renderItem: React.FC<{item: any; index: any}> = ({item, index}) => (
  <Card onPress={() => {showDetail()}} style={styles.cardLayout} status={statusColor(item.status)}>
    <Layout style={styles.cardContent}>
      <Text style={{textAlignVertical: 'center', marginRight: 12}} category="h5">{index + 1}</Text>
      <IvmiImageIcon style={styles.vegetableThumbnail} src={{uri: vegetableThumbnail[item.thumbnail]}} size={48} />
      <Layout style={{marginLeft: 12, width: '100%'}}>
        <Text category="h5">{item.vegetable}</Text>
        <Text>
          <IvmiImageIcon src={require('../../assets/icons/archive.png')} size={20} /> {item.available}/{item.plots}
        </Text>
      </Layout>
    </Layout>

    { (item.status == "Disease" || item.status == "Harvest") &&
      <Layout>
        <Divider style={{marginVertical: 12}}/>
        <Text>
          {item.notification}
        </Text>
      </Layout>
    }
    
  </Card>
);

const Garden = () => {
  return (
    <Layout level="2" style={styles.container}>
      <Text style={{paddingTop: 24, paddingBottom: 12, fontWeight: '700'}} category="h4">Good morning Hoang Son!</Text>
      <Text style={
        {textAlign: 'left', width: '100%', marginLeft: 24, fontWeight: '700'}
        }
        category="h6">Your plots ({data.length})</Text>
      <List
        style={{flex: 1, width: '100%'}}
        data={data}
        renderItem={renderItem}
      />
      <Layout
        level="2"
        style={{
          height: 100,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Button
          style={{
            borderRadius: 30,
            // width: 130,
            // height: 60,
            // paddingHorizontal: 0,
          }}
          appearance='outline'
          size="giant">
          +  Add more plots
        </Button>
      </Layout>
    </Layout>
  );
};

const showDetail = () => {
  
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    // paddingTop: 35,
  },
  cardLayout: {
    margin: 12,
    borderRadius: 12,
    padding: 0
  },
  cardContent: {
    flex: 1,
    flexDirection: "row"
  },
  vegetableThumbnail: {
    borderRadius: 10,
    overflow: "hidden",
    borderWidth: 1,
    borderColor: "#ccc"},
});

const statusColor = (status: string) => {
  switch (status) {
    case "Harvest":
      return "success"
      break;
    case "Disease":
        return "danger"
        break;
  }
}

export default Garden;
