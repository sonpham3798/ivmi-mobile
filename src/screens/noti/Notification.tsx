import React, {useState} from 'react';
import {
  Layout,
  List,
  Select,
  SelectItem,
  IndexPath,
  Modal,
  Card,
  Button,
  Divider,
} from '@ui-kitten/components';
import IvmiNotiItem from '../../components/IvmiNotiItem';
import {TouchableOpacity, View, Text} from 'react-native';
import IvmiText from '../../components/IvmiText';
import IvmiImageIcon from '../../components/IvmiImageIcon';
import IvmiIcon from '../../components/IvmiIcon';

interface INofitication {
  type: string;
  line: number;
  pos: number;
  id: number;
  time: string;
  value: string;
  unread: boolean;
}

const data: INofitication[] = [
  {
    type: 'Harvest',
    line: 1,
    pos: 3,
    id: 1,
    time: '06/06/2020 13:57',
    value: 'Salad seems to be able to harvest :D',
    unread: false,
  },
  {
    type: 'Drills',
    line: 1,
    pos: 3,
    id: 1,
    time: '06/06/2020 13:57',
    value: 'hihi',
    unread: false,
  },
  {
    type: 'Harvest',
    line: 1,
    pos: 3,
    id: 1,
    time: '06/06/2020 13:57',
    value: 'Salad seems to be able to harvest :D',
    unread: false,
  },
  {
    type: 'Diseases',
    line: 1,
    pos: 3,
    id: 1,
    time: '06/06/2020 13:57',
    value: 'Broccoli may be have yellow leaves diseases :(',
    unread: true,
  },
  {
    type: 'Diseases',
    line: 1,
    pos: 3,
    id: 1,
    time: '06/06/2020 13:57',
    value: 'Broccoli may be have yellow leaves diseases :(',
    unread: false,
  },
  {
    type: 'Harvest',
    line: 1,
    pos: 3,
    id: 1,
    time: '06/06/2020 13:57',
    value: 'Salad seems to be able to harvest :D',
    unread: false,
  },
  {
    type: 'Harvest',
    line: 1,
    pos: 3,
    id: 1,
    time: '06/06/2020 13:57',
    value: 'Salad seems to be able to harvest :D',
    unread: false,
  },
  {
    type: 'Harvest',
    line: 1,
    pos: 3,
    id: 1,
    time: '06/06/2020 13:57',
    value: 'Salad seems to be able to harvest :D',
    unread: false,
  },
  {
    type: 'Harvest',
    line: 1,
    pos: 3,
    id: 1,
    time: '06/06/2020 13:57',
    value: 'Salad seems to be able to harvest :D',
    unread: false,
  },
];

const Notification = () => {
  const [selectedIndex, setSelectedIndex] = useState<any>(new IndexPath(1));
  const selectData = ['Unread', 'Diseases', 'Harvest', 'Drills'];
  const [visible, setVisible] = useState(false);

  const renderItem: React.FC<{item: INofitication; index: number}> = ({
    item,
    index,
  }) => (
    <View style={{width: '95%', alignSelf: 'center', marginBottom: 10}}>
      <TouchableOpacity onPress={() => setVisible(!visible)}>
        <IvmiNotiItem
          key={index}
          icon={
            <IvmiImageIcon
              src={
                item.type == 'Diseases' && item.unread
                  ? require('../../../assets/icons/diseases.png')
                  : item.type == 'Diseases' && !item.unread
                  ? require('../../../assets/icons/read-diseases.png')
                  : item.type == 'Harvest'
                  ? require('../../../assets/icons/harvest.png')
                  : require('../../../assets/icons/drills.png')
              }
            />
          }
          title={`${item.type} - Line ${item.line} - Position: ${item.pos}`}
          time={item.time}
          content={item.value}
          unread={item.unread}
        />
      </TouchableOpacity>
    </View>
  );

  return (
    <Layout style={{flex: 1, alignItems: 'center'}}>
      <Layout
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'space-between',
          width: '88%',
          paddingVertical: 10,
        }}>
        <Layout
          style={{
            flexDirection: 'row',
            alignItems: 'center',
          }}>
          <IvmiText>Filter: </IvmiText>
          <Select
            style={{width: 150}}
            size="small"
            selectedIndex={selectedIndex}
            value={() => <IvmiText>{selectData[selectedIndex.row]}</IvmiText>}
            onSelect={(index) => setSelectedIndex(index)}>
            <SelectItem title={() => <IvmiText>Unread</IvmiText>} />
            <SelectItem title={() => <IvmiText>Diseases</IvmiText>} />
            <SelectItem title={() => <IvmiText>Harvest</IvmiText>} />
            <SelectItem title={() => <IvmiText>Drills</IvmiText>} />
          </Select>
        </Layout>
        <TouchableOpacity
          style={{
            width: 30,
            height: 30,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <IvmiImageIcon
            src={require('../../../assets/icons/done-all.png')}
            size={20}
          />
        </TouchableOpacity>
      </Layout>
      <List
        style={{flex: 1, width: '100%'}}
        data={data}
        renderItem={renderItem}
      />
      <Modal
        visible={visible}
        backdropStyle={{backgroundColor: 'rgba(0, 0, 0, 0.5)'}}>
        <Card
          disabled={true}
          status="info"
          style={{maxWidth: '90%', alignSelf: 'center'}}>
          <IvmiText style={{alignSelf: 'center'}} fontStyle="medium">
            Diseases - Line 1 - Position: 3
          </IvmiText>
          <Divider
            style={{
              backgroundColor: 'rgba(0,0,0,0.2)',
              marginVertical: 3,
              width: '100%',
            }}
          />
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            <IvmiIcon name="arrow-right-outline" size={17} color="gray" />
            <IvmiText>Tree: Salad</IvmiText>
          </View>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            <IvmiIcon name="arrow-right-outline" size={17} color="gray" />
            <IvmiText>Age: About 7 days life</IvmiText>
          </View>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            <IvmiIcon name="arrow-right-outline" size={17} color="gray" />
            <IvmiText>Status: Maybe yellow leaves</IvmiText>
          </View>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              marginTop: 30,
              marginBottom: 5,
            }}>
            <IvmiIcon name="info-outline" color="gray" />
            <IvmiText>Tips</IvmiText>
          </View>
          <IvmiText>You can check it and confirm resolved after that.</IvmiText>
          <Button style={{marginTop: 15}} onPress={() => setVisible(false)}>
            CONFIRM
          </Button>
        </Card>
      </Modal>
    </Layout>
  );
};

export default Notification;
