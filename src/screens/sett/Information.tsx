import React from 'react';
import {View, Text} from 'react-native';
import IvmiText from '../../components/IvmiText';
import {Button} from '@ui-kitten/components';
import {useNavigation} from '@react-navigation/native';

const Information = () => {
  const navigation = useNavigation();
  return (
    <View
      style={{
        paddingHorizontal: '5%',
        paddingTop: 17,
        height: '100%',
        justifyContent: 'space-evenly',
      }}>
      <IvmiText>FPT University - Ho Chi Minh</IvmiText>
      <IvmiText>Capstone project:</IvmiText>
      <IvmiText>
        - Product Name: Intelligent vegetable garden management system using IP
        camera
      </IvmiText>
      <IvmiText>- Product Code: IVMI</IvmiText>
      <IvmiText>Supervisor:</IvmiText>
      <IvmiText>- Kieu Trong Khanh</IvmiText>
      <IvmiText>- Doan Nguyen Thanh Hoa</IvmiText>
      <Button
        style={{marginTop: 15}}
        onPress={() => navigation.navigate('Settings')}>
        GO BACK
      </Button>
    </View>
  );
};

export default Information;
