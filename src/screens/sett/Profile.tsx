import React from 'react';
import {View, Text} from 'react-native';
import {Input, Button} from '@ui-kitten/components';
import IvmiText from '../../components/IvmiText';
import {useNavigation} from '@react-navigation/native';

const Profile = () => {
  const navigation = useNavigation();
  return (
    <View style={{flex: 1, paddingHorizontal: '5%', paddingTop: 17}}>
      <IvmiText style={{alignSelf: 'center'}}>Your profile</IvmiText>
      <Input label="Username" placeholder="Place your Text" />
      <Input label="Full name" placeholder="Place your Text" />
      <Input label="Address" placeholder="Place your Text" />
      <Input label="Email" placeholder="Place your Text" />
      <Input label="Phone" placeholder="Place your Text" />
      <Button
        style={{marginTop: 15}}
        onPress={() => navigation.navigate('Settings')}>
        SAVE
      </Button>
    </View>
  );
};

export default Profile;
