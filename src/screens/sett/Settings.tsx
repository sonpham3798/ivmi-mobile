import React from 'react';
import {View, ImageBackground} from 'react-native';
import {Layout} from '@ui-kitten/components';
import IvmiProfileItem from '../../components/IvmiProfileItem';
import IvmiText from '../../components/IvmiText';
import IvmiImageIcon from '../../components/IvmiImageIcon';

const Settings = () => {
  return (
    <>
      <Layout style={{flex: 1, alignItems: 'center'}}>
        {/* image */}
        <View
          style={{
            width: '100%',
            paddingHorizontal: '5%',
            paddingTop: 17,
            backgroundColor: 'white',
            shadowColor: '#000',
            shadowOffset: {
              width: 0,
              height: 9,
            },
            shadowOpacity: 0.5,
            shadowRadius: 12.35,

            elevation: 19,
          }}>
          <IvmiProfileItem
            to="Profile"
            name="Profile"
            leftIcon={
              // <IvmiIcon name="person-outline" size={25} color="#00E096" />
              <IvmiImageIcon
                src={require('../../../assets/icons/profile.png')}
                size={20}
              />
            }
          />
          <IvmiProfileItem
            to="Profile"
            name="Notification"
            leftIcon={
              <IvmiImageIcon
                src={require('../../../assets/icons/noti.png')}
                size={20}
              />
            }
          />
          <IvmiProfileItem
            to="Profile"
            name="Help"
            leftIcon={
              <IvmiImageIcon
                src={require('../../../assets/icons/help.png')}
                size={20}
              />
            }
          />
          <IvmiProfileItem
            to="Information"
            name="Information"
            leftIcon={
              <IvmiImageIcon
                src={require('../../../assets/icons/information.png')}
                size={20}
              />
            }
          />
        </View>
        <View
          style={{
            width: '100%',
            height: 250,
            paddingHorizontal: '5%',
            marginTop: 10,
            backgroundColor: 'white',
            shadowColor: '#000',
            shadowOffset: {
              width: 0,
              height: -19,
            },
            shadowOpacity: 0.5,
            shadowRadius: 12.35,

            elevation: 19,
          }}>
          <IvmiProfileItem to="Login" name="Sign out" />
        </View>
      </Layout>

      {/* <Layout
        level="3"
        style={{width: '95%', height: 200, borderRadius: 15, padding: 20}}>
        <Text category="h6" style={{marginBottom: 10}}>
          Garden: Thang Luong
        </Text>
        <Text category="h6" style={{marginBottom: 10}}>
          2 Lines - 5 per line
        </Text>
        <Text category="h6" style={{marginBottom: 10}}>
          Service contacts:
        </Text>
        <Text category="h6" style={{marginBottom: 10}}>
          Support: 0912345678
        </Text>
        <Text category="h6" style={{marginBottom: 10}}>
          Sale: 0987654321
        </Text>
      </Layout>
      <Divider style={{backgroundColor: 'rgba(0,0,0,0.1)', width: '90%'}} />
      <Button
        accessoryLeft={InfoIcon}
        style={{
          width: '95%',
          height: 50,
          borderRadius: 12,
          justifyContent: 'flex-start',
        }}
        size="large"
        appearance="ghost">
        Profile
      </Button>
      <Divider style={{backgroundColor: 'rgba(0,0,0,0.1)', width: '90%'}} />
      <Button
        accessoryLeft={SettingsIcon}
        style={{
          width: '95%',
          height: 50,
          borderRadius: 12,
          justifyContent: 'flex-start',
        }}
        size="large"
        appearance="ghost">
        Settings
      </Button>
      <Divider style={{backgroundColor: 'rgba(0,0,0,0.1)', width: '90%'}} />
      <Layout
        style={{
          marginTop: 10,
          alignSelf: 'flex-end',
          width: '90%',
        }}>
        <TouchableOpacity>
          <Text>Log out?</Text>
        </TouchableOpacity>
      </Layout> */}
    </>
  );
};

export default Settings;
